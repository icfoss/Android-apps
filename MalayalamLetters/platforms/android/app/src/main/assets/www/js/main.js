$(document).ready(function(){

		 vowels=["അ","ആ","ഇ","ഈ","ഉ","ഊ","ഋ","എ","ഏ","ഐ","ഒ","ഓ","ഔ","അം","അ:"];
	 
		vowelsarr =["അ","ആ","ഇ","ഈ","ഉ","ഊ","ഋ","എ","ഏ","ഐ","ഒ","ഓ","ഔ","അം","അ:"];
		
			function findIndex(){
				    IndexValue=0;
				    
					
					}	
		
		$("#level1,#level2,#scoreboard").hide();
		
		$("#home").hide();
		
		$("#button1").click(function(){
		
			$("#buttonclick").trigger("play");
		    level1();
		});
		
		
		function level1(){
		
			$("#timer").TimeCircles().destroy();
			
			$("#scoreboard").text("തെറ്റ്/0");
			
			$("#inputvowels,.box,#scoreboard").show();
			
			$("#home").show();
			
			$("#falert2,#inputconsonants,#falert1").hide();
			
			$("#screen").text("ബട്ടണ്‍ ക്ലിക്ക് ചെയ്ത്  സ്വരാക്ഷരങ്ങളെ ക്രമീകരിക്കുക");
			
			$("header h2").text("സ്വരാക്ഷാരങ്ങളുടെ ക്രമീകരണം");
			
			/* data = $.getJSON("json/sorting.json",function(result){
			
				res = data.responseJSON;
				
				console.log(res);
			});*/
			
			$("#start").fadeOut(function(){
			
				$("#level1").fadeIn();
				
			});
			
			function randOrd(){
			
  				return (Math.round(Math.random())-0.05);
  				
			}
			
			vowels.sort(randOrd);
				
			for(var i=0; i<vowels.length;i++){
			
				$("#box"+(i+1)).text(vowels[i]).data("text",vowels[i]);
				
				
				
			}
			
			correct=0;
			
    		wrong=0;
    			
			count=0;
			
			counter=0;
			
			$("#home").click(function(){
				
				
    			
    			$("#buttonclick").trigger("play");
				
				$("#startPage").show();
				
				//alert("click");
				
				document.location="index.html";
				
				});
			
					
			
			$(".box").click(function(){
		
				$("#timer").TimeCircles({ circle_bg_color: "#ffffff", time: { Days: { show: false }, Hours: { show: false } }}).start();
			
				count++;
			
				if(count==1){
			
					$("#screen").empty();
				
				}
			
				value=$(this).data("text");
			
				sequence=vowelsarr[counter];
				
				console.log("value="+value+" sequence="+sequence)
			
				if(value==sequence){
			$("#buttonclick").trigger("play");
					correct++;					
					IndexValue=vowelsarr.findIndex((x => x == sequence)); 
					IndexValue=IndexValue+1;					
					//$("#correctclip").trigger("play");
					$("#sound").attr('src',"sound/vowels/"+IndexValue+".ogg").trigger("play");
					$("#screen").append("<div class='lettersv'>"+value+"</div>");
			        
					$(this).hide();
				
					counter++;
					
					
				
				}else{
			
					wrong++;
					
					
				
					$("#scoreboard").text("തെറ്റ്/"+wrong);
					
					$("#wrongclip").trigger("play");
				
					alert("തെറ്റായ ക്രമീകരണം ");
					
					
				
				}
			
				if(counter==vowels.length){
			
					$("#falert1").fadeIn();
					
					$("#completed").trigger("play");
					
					$("#inputvowels").hide();
					
					acc=100-((wrong/vowels.length) * 100);
					
					accuracy=acc.toFixed(2);
					
					$("#scoreboard").text("Accuracy="+accuracy+"%");
					
					$("#timer").TimeCircles().stop();
					
					$(".box").unbind();
					
    			}
    		
    		});
    		
		}
		
		$("#retry1").click(function(){
			$("#buttonclick").trigger("play");
			level1();
		});
		
		
		$("#button2").click(function(){
			$("#buttonclick").trigger("play");
		  	level2();
		});
		 
	    $("#retry2").click(function(){
	    	$("#buttonclick").trigger("play");
			level2();
			$("#inputconsonants").show();
		});
		
		 $("#btn_level1").click(function(){
		 	$("#buttonclick").trigger("play");
			level1();	
		});
		
		$("#btn_level2").click(function(){
			$("#buttonclick").trigger("play");
			level2();
		});
		
		function level2(){
		
		consonants=["ക","ഖ","ഗ","ഘ","ങ","ച","ഛ","ജ","ഝ","ഞ","ട","ഠ","ഡ","ഢ","ണ","ത","ഥ","ദ","ധ","ന","പ","ഫ","ബ","ഭ","മ","യ","ര","ല","വ","ശ","ഷ","സ","ഹ","ള","ഴ","റ"];
		
	consonantsarr=["ക","ഖ","ഗ","ഘ","ങ","ച","ഛ","ജ","ഝ","ഞ","ട","ഠ","ഡ","ഢ","ണ","ത","ഥ","ദ","ധ","ന","പ","ഫ","ബ","ഭ","മ","യ","ര","ല","വ","ശ","ഷ","സ","ഹ","ള","ഴ","റ"];
			$("#timer").TimeCircles().destroy();
			
			$("header h2").text("വ്യഞ്ജനാക്ഷാരങ്ങളുടെ ക്രമീകരണം");
			
			$("#screen").text("ബട്ടണ്‍ ക്ലിക്ക് ചെയ്ത്  വ്യഞ്ജനാക്ഷാരങ്ങളെ ക്രമീകരിക്കുക");
			
			$("#scoreboard").text("തെറ്റ്/0")
			
			$("#home").show();
			
			$("#inputconsonants,.cbox,#scoreboard").show();
			
			$("#falert2,#inputvowels,#falert1").hide();
			
			$("#start").fadeOut(function(){
			
				$("#level1").fadeIn();
				
			});
			
			
			
			function randOrd(){
			
  				return (Math.round(Math.random())-0.05);
  				
			}
				consonants.sort(randOrd);
				
			for(var i=0; i<consonants.length;i++){
			
				$("#cbox"+(i+1)).text(consonants[i]);
				
				$("#cbox"+(i+1)).data("text",consonants[i]);
				
			}
			
			correct=0;
			
    		wrong=0;
    		
			count=0;
			
			index=0;
			
			$("#home").click(function(){
				
				$("#buttonclick").trigger("play");
				
				time=$("#buttonclick").prop("played").length;
				$("#startPage").show();
			
				document.location="index.html";
				
				});
			
		$(".cbox").click(function(){
		
			$("#timer").TimeCircles({ circle_bg_color: "#ffffff", time: { Days: { show: false }, Hours: { show: false } }}).start();
			
			count++;
			
			if(count==1){
			
				$("#screen").empty();
				
			}
			
			
			value=$(this).data("text");
			
			csequence=consonantsarr[index];
			
			if(value==csequence){
			
			correct++;
			IndexValue=consonantsarr.findIndex((x => x == csequence)); 
			IndexValue=IndexValue+1;	
			$("#buttonclick").trigger("play");				
			//$("#correctclip").trigger("play");
			$("#sound").attr('src',"sound/consonants/"+IndexValue+".ogg").trigger("play");
			$("#screen").append("<div class='letters'>"+value+"</div>");
			
			$(this).hide();
			
			$("#correctclip").trigger("play");
			
			index++;
			
			}else{
			
				wrong++;
				
				$("#scoreboard").text("തെറ്റ്/"+wrong);
				
				$("#wrongclip").trigger("play");
				
				alert("തെറ്റായ ക്രമീകരണം ");
				
			}
			
			if(index==consonants.length){
			
				$("#falert2").fadeIn();
				
				$("#completed").trigger("play");
				
    			$("#inputconsonants").hide();
    			
    			acc=100-((wrong/consonants.length) * 100);
    			
    			accuracy=acc.toFixed(2);
    			
    			$("#scoreboard").text("Accuracy="+accuracy+"%");
    			
    			$("#timer").TimeCircles().stop();
    			
    			$(".cbox").unbind();
    		}
    		
    		
				
    		});
		}
		
		
			
		
	});
    		
    			
